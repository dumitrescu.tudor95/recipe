package tudor.repositories;

import org.springframework.data.repository.CrudRepository;
import tudor.domain.Recipe;

public interface RecipeRepository extends CrudRepository<Recipe,Long> {
}
