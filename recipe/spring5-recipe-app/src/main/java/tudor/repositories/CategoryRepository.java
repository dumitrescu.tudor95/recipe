package tudor.repositories;

import org.springframework.data.repository.CrudRepository;
import tudor.domain.Category;

import java.util.Optional;

public interface CategoryRepository extends CrudRepository<Category,Long> {

    Optional<Category> findByDescription(String description);

}
