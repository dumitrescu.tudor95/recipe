package tudor.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private Integer prepTime;

    @Getter
    @Setter
    private Integer cookTime;

    @Getter
    @Setter
    private Integer servings;

    @Getter
    @Setter
    private String source;

    @Getter
    @Setter
    private String url;

    @Getter
    @Setter
    @Lob
    private String directions;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "recipe")
    @Getter
    @Setter
    private Set<Ingredient> ingredients=new HashSet<>();

    @Getter
    @Setter
    @Lob
    private Byte[] image;

    @Enumerated(value= EnumType.STRING)
    @Getter
    @Setter
    private Difficulty difficulty;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.ALL)
    private Notes notes;

    @Getter
    @Setter
    @ManyToMany
    @JoinTable(name="recipe_category",
    joinColumns = @JoinColumn(name="recipe_id"),
            inverseJoinColumns = @JoinColumn(name="category_id"))
    private Set<Category> categories =new HashSet<>();
}
